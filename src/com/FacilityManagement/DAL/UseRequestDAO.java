package com.FacilityManagement.DAL;



import com.FacilityManagement.Model.BridgePattern.Request.UserRequest;
import com.FacilityManagement.Model.BridgePattern.Schedule.ISchedule;
import com.FacilityManagement.Model.BridgePattern.Schedule.Schedule;
import com.FacilityManagement.Model.Technology.Technology;
import com.FacilityManagement.Model.Technology.iTechnology;
import com.FacilityManagement.Model.VisitorPattern.User.User.IUser;
import com.FacilityManagement.Model.VisitorPattern.User.User.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;


public class UseRequestDAO {
    private ScheduleDAO scheduleDAO = new ScheduleDAO(); //Spring
    private UserDAO userDAO = new UserDAO();
    private int ID;

    public int getID() {
        return ID;
    }

    public void setScheduleDAO(ScheduleDAO scheduleDAO) {
        this.scheduleDAO = scheduleDAO;
    }

    public void removeAllData() {
        try {
            Statement st = DB_Helper.getConnection().createStatement();
            String removeUseRequestQuery = "delete from userequest";
            String removeUseSchedule = "delete from schedule where requesttype = " + "'" + "Use" + "'";
            st.execute(removeUseRequestQuery);
            st.execute(removeUseSchedule);

            System.out.println("UseRequest DAO: *************** Query " + removeUseRequestQuery + "\n");
            st.close();
        } catch (SQLException se) {
            System.err.println("UseRequestDAO: Threw a SQLException removing the UseRequest object from UseRequest table.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
    }

    public void removeUseRequest(UserRequest useRequest) {
        try {
            Statement st = DB_Helper.getConnection().createStatement();
            String removeUseRequestQuery = "delete from userequest where scheduleID = '" + useRequest.getSchedule().getScheduleID() + "'";
            st.execute(removeUseRequestQuery);
            String removeScheduleQuery = "delete from schedule where scheduleID = '" + useRequest.getSchedule().getScheduleID() + "'";
            st.execute(removeScheduleQuery);
            System.out.println("UserRequest DAO: *************** Query " + removeUseRequestQuery + "\n");
            System.out.println("Schedule DAO: *************** Query " + removeScheduleQuery + "\n");
            st.close();
        } catch (SQLException se) {
            System.err.println("UseRequestDAO: Threw a SQLException removing the UseRequest object from UseRequest table.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }

    }

    public void AddUseRequest(UserRequest useRequest) {

            Connection con = DB_Helper.getConnection();
            PreparedStatement addUse = null;
            Random rand = new Random(); //Spring ?

            try {
                //Insert the UseService object

                //int id = rand.nextInt(100);


                //useRequest.setUseRequestID(id2);
                scheduleDAO.AssignFacilityToUse(useRequest.getSchedule());
                System.out.println(useRequest.getSchedule().getScheduleID());

                //Insert the facility_detail object
                String addReq = "INSERT INTO UseRequest(UseRequestID, FacilityID, ScheduleID, RoomNumber,useDescription,EquipmentType ,EquipmentCode, UserID ) VALUES(?, ?, ?, ?,?,?,?,?)";
                addUse = con.prepareStatement(addReq);
                addUse.setInt(1, useRequest.getRequestID());
                addUse.setInt(2, useRequest.getSchedule().getFacilityID());
                addUse.setInt(3, useRequest.getSchedule().getScheduleID());
                addUse.setInt(4, useRequest.getSchedule().getRoomNumber());
                addUse.setString(5, useRequest.getRequestDescription());
                addUse.setString(6, useRequest.getTechnology().ListoftechType());
                addUse.setString(7, useRequest.getTechnology().ListoftechCode());
                addUse.setInt(8, useRequest.getiUser().getUserID());



                addUse.executeUpdate();

            } catch (SQLException ex) {
                System.out.println(ex);

            } finally {

                try {
                    if (addUse != null) {
                        addUse.close();
                    }
                    if (con != null) {
                        con.close();
                    }

                } catch (SQLException ex) {
                    System.err.println("UseRequestDAO: Threw a SQLException saving the UseRequest object.");
                    System.err.println(ex.getMessage());
                }

            }


        }


    public boolean isInUseDuringInterval(UserRequest ins) {
        if (!scheduleDAO.isInUseDuringInterval(ins.getSchedule())){
            return false;
        }
        ID = scheduleDAO.getID();
        return true;

    }
    public void VacateInsFacility(UserRequest ins) {
        scheduleDAO.VacateFacility(ins.getSchedule());
    }


    public UserRequest getInfobyID(int ID) {
        try {

            Statement st = DB_Helper.getConnection().createStatement();
            String selectDetailQuery = "SELECT *  FROM userequest,Schedule WHERE Schedule.ScheduleID= '" + ID + "' AND userequest.ScheduleID = Schedule.ScheduleID ";
            ResultSet detRS = st.executeQuery(selectDetailQuery);

            return achieveuser(detRS, selectDetailQuery);
        }

        catch (SQLException se) {
            System.err.println("FacilityDAO: Threw a SQLException retrieving the Facility object.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }

        return null;
    }

    private UserRequest achieveuser(ResultSet detRS, String selectDetailQuery) {
        try {
            UserRequest userRequest = new UserRequest();
            FacilityDAO facilityDAO = new FacilityDAO();
            ISchedule schedule = new Schedule();
            iTechnology technology = new Technology();
            IUser user = new User();




            System.out.println("UserequestDAO: *************** Query " + selectDetailQuery + "\n");

            while ( detRS.next() ) {
                userRequest.setRequestID(detRS.getInt("userequestid"));
                userRequest.setRequestDescription(detRS.getString("usedescription"));

                String code = detRS.getString("equipmentcode");
                String type = detRS.getString("equipmenttype");
                technology.setTechnologytype( new ArrayList<String>(Arrays.asList(type.split(","))));
                List<String> listcode = new ArrayList<String>(Arrays.asList(code.split(" ,")));
                ArrayList<Integer> codereal = new ArrayList<Integer>();
                for(String s : listcode) codereal.add(Integer.valueOf(s));
                technology.setTechnologycode(codereal);

              user = userDAO.getInformationByuserID(detRS.getInt("UserID"));

                schedule.setRequestType("requesttype");
                schedule.setFacility(facilityDAO.getFacilityInformationByFacilityID(detRS.getInt("facilityid")));
                schedule.setScheduleID(detRS.getInt("scheduleid"));
                schedule.setRoomNumber(detRS.getInt("roomnumber"));
                schedule.setStartDate(detRS.getDate("startdate").toLocalDate());
                schedule.setEndDate(detRS.getDate("enddate").toLocalDate());
            }

            userRequest.setSchedule(schedule);
            userRequest.setiUser(user);
            userRequest.setTechnology(technology);

            //close to manage resources
            detRS.close();

            return userRequest;
        } catch (SQLException se) {
            System.err.println("userrequestDAO: Threw a SQLException retrieving the request object.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }

        return null;

    }

}

        //}


