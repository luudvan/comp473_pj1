package com.FacilityManagement.Model.VisitorPattern.User.Usertype;

import com.FacilityManagement.Model.BridgePattern.Request.UserRequest;
import com.FacilityManagement.Model.BridgePattern.Schedule.ISchedule;
import com.FacilityManagement.Model.Technology.iTechnology;
import com.FacilityManagement.Model.VisitorPattern.Facility.Facility;
import com.FacilityManagement.Model.VisitorPattern.Facility.IFacility;
import com.FacilityManagement.Model.VisitorPattern.User.User.IUser;


public class Faculty implements IVisitor {


    private IUser user;
    private UserRequest userRequest = new UserRequest();
    private IFacility facility = new Facility();


    public IUser getUser() {
        return user;
    }

    public void setUser(IUser user) {
        this.user = user;
    }

    public UserRequest use(iTechnology iTechnology, ISchedule schedule, String RequestDes) {
        userRequest.setiUser(user);
        userRequest.setRequestDescription(RequestDes);
        userRequest.setTechnology(iTechnology);
        schedule.setFacility(facility);
        userRequest.setSchedule(schedule);
        return userRequest;

    }

    public void visitFacility(IFacility facility) {
        this.facility = facility;
    }


}
