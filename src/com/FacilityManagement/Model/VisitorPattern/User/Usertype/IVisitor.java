package com.FacilityManagement.Model.VisitorPattern.User.Usertype;

import com.FacilityManagement.Model.VisitorPattern.Facility.IFacility;

public interface IVisitor {
    public void visitFacility(IFacility facility);
}
