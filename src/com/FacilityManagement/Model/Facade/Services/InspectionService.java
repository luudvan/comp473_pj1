package com.FacilityManagement.Model.Facade.Services;

import com.FacilityManagement.DAL.InspectionDAO;
import com.FacilityManagement.DAL.ScheduleDAO;
import com.FacilityManagement.DAL.UserDAO;
import com.FacilityManagement.Model.BridgePattern.Request.IRequest;
import com.FacilityManagement.Model.BridgePattern.Request.InspectionRequest;
import com.FacilityManagement.Model.BridgePattern.Schedule.ISchedule;
import com.FacilityManagement.Model.VisitorPattern.User.User.IUser;
import com.FacilityManagement.Model.VisitorPattern.User.User.User;

import java.util.Scanner;

public class InspectionService {


    private InspectionDAO inspectionDAO = new InspectionDAO();
    private UserDAO userDAO = new UserDAO();
    private InspectionRequest inspection = new InspectionRequest();
    private IUser user = new User();
    private Scanner scanner =  new Scanner(System.in);
    private ScheduleDAO scheduleDAO = new ScheduleDAO();



    /*private InspectionRequest inspectionRequest ;
    private InspectionDAO inspectionDAO ;



    //spring
    public InspectionRequest getInspectionRequest() {
        return inspectionRequest;
    }

    public void setInspectionRequest(InspectionRequest inspectionRequest) {
        this.inspectionRequest = inspectionRequest;
    }

    public InspectionDAO getInspectionDAO() {
        return inspectionDAO;
    }

    public void setInspectionDAO(InspectionDAO inspectionDAO) {
        this.inspectionDAO = inspectionDAO;
    }
    //spring*/


    public void AddRequest(InspectionRequest inspectionRequest) {
        user = userDAO.getInformationByuserID(inspectionRequest.getiUser().getUserID());
        if (!(inspectionDAO.isInUseDuringInterval(inspectionRequest))) {
            inspectionRequest.getSchedule().setRequestType("Inspection");
            inspectionDAO.AddInsRequest(inspectionRequest);
            System.out.println("Successfully added your request");
        }
        else {
            Subscribe();
        }

    }


    public void isInUseDuringInterval(InspectionRequest inspectionRequest) {
        inspectionDAO.isInUseDuringInterval(inspectionRequest);
    }


    public void RemoveRequest(int ID) {
        inspection =inspectionDAO.getInfobyID(ID);
        inspection.getSchedule().getObservable().notifyObserver();
        inspectionDAO.removeInspectionRequest(inspection);
    }


    public void VacateFacility(int ID) {
        inspection=inspectionDAO.getInfobyID(ID);
        inspectionDAO.VacateInsFacility(inspection);
    }


    public void clearAllUseData() {
        inspectionDAO.removeAllData();
    }


    public IRequest visitgetInfobyID(int ID) {
        inspection =inspectionDAO.getInfobyID(ID);
        return inspection;
    }


    //Observer
    private void Subscribe(){
        System.out.println("Sorry this room is not available in this time, " +
                "would you like to get notified when its available in this time? Y/N ");
        String answer = scanner.nextLine();
        if(answer.equals("Y")){
            ISchedule schedule = scheduleDAO.getInfobyID(inspectionDAO.getID());
            scheduleDAO.addSubscriber(schedule,user);


            System.out.println("You will be notified when the room is available in the selected time via your choice of service");
        }
        else if(answer.equals("N")){
            System.out.println("Please choose another room");

        }
        else {
            System.out.println("Wrong input, please try again");
            Subscribe();
        }


    }
}
