package com.FacilityManagement.View;

import com.FacilityManagement.Model.BridgePattern.Request.InspectionRequest;
import com.FacilityManagement.Model.BridgePattern.Schedule.ISchedule;
import com.FacilityManagement.Model.BridgePattern.Schedule.Schedule;
import com.FacilityManagement.Model.Technology.Technology;
import com.FacilityManagement.Model.Technology.iTechnology;
import com.FacilityManagement.Model.VisitorPattern.User.User.IUser;
import com.FacilityManagement.Model.VisitorPattern.User.IUserService;
import com.FacilityManagement.Model.VisitorPattern.User.UserService;
import com.FacilityManagement.Model.VisitorPattern.User.Usertype.Inspector;
import com.FacilityManagement.Model.VisitorPattern.User.User.User;
import com.FacilityManagement.Model.VisitorPattern.Facility.FacilityService;
import com.FacilityManagement.Model.VisitorPattern.Facility.IFacility;
import com.FacilityManagement.Model.VisitorPattern.Facility.IFacilityService;
import com.FacilityManagement.Model.ObserverPattern.ObserverNotify.IObserver;
import com.FacilityManagement.Model.ObserverPattern.ObserverNotify.viaSMS;
import com.FacilityManagement.Model.ObserverPattern.Subscriber.IObservable;
import com.FacilityManagement.Model.ObserverPattern.Subscriber.Observable;
import com.FacilityManagement.Model.Facade.Services.InspectionService;

import java.time.LocalDate;

public class InspectionClient {

    private IFacilityService facilityService = new FacilityService();
    private InspectionService InspectionService =new InspectionService();

    private IUserService userService = new UserService();

    private ISchedule schedule = new Schedule();
    private IUser user = new User();
    private Inspector inspector = new Inspector();
    private iTechnology technology = new Technology();
    private InspectionRequest inspectionRequest = new InspectionRequest();


    private IObserver observer = new viaSMS();
    private IObservable observable = new Observable();


   /* //spring
    private IFacilityService facilityService ;
    private InspectionService InspectionService ;
    private ISchedule schedule;
    private Inspector inspector ;
    private iTechnology technology;
    private clearallUseData clearallUseData;
    private AddRequest addRequest;
    private removeRequest removeRequest;
    private vacateFacility vacateFacility;
    private isInUseDuringInterval isInUseDuringInterval;

    public InspectionClient(IFacilityService facilityService, InspectionService inspectionService, ISchedule schedule, Inspector inspector, iTechnology technology, clearallUseData clearallUseData, AddRequest addRequest, removeRequest removeRequest, vacateFacility vacateFacility, isInUseDuringInterval isInUseDuringInterval) {
        this.facilityService = facilityService;
        InspectionService = inspectionService;
        this.schedule = schedule;
        this.inspector = inspector;
        this.technology = technology;
        this.clearallUseData = clearallUseData;
        this.addRequest = addRequest;
        this.removeRequest = removeRequest;
        this.vacateFacility = vacateFacility;
        this.isInUseDuringInterval = isInUseDuringInterval;
    }
*/

    public void start(){
        System.out.println("\nInspectionClient: *************** Clearing All Data *************************");
        InspectionService.clearAllUseData();
        //---------------------------------------------------------------

        System.out.println("\nInspectionClient: *************** Instantiating InspectionRequest *************************");
        IFacility fac = facilityService.getFacilityInformation("Law Center");
        user = userService.getUserInformation(2);
        inspector.setUser(user);
        fac.accept(inspector);
        schedule.setRoomNumber(8);
        schedule.setStartDate(LocalDate.of(2020,05,01) );
        schedule.setEndDate(LocalDate.of(2020,05,04));
        schedule.setObservable(observable);

        technology.addtechnology(20116,"PC Computer");
        technology.addtechnology(20134,"Projector");

        inspectionRequest= inspector.inspection(technology,schedule,"Daily Checkup");


        InspectionService.AddRequest(inspectionRequest);

        System.out.println("\nInspectionClient: *************** Vacate a Facility in Inspection in Schedule (Shorten Time)  *************************");
        InspectionService.VacateFacility(5);


        System.out.println("\nInspectionClient: *************** Remove a Facility  from InspectionRequest and Schedule *************************");
        InspectionService.RemoveRequest(5);
    }
}
