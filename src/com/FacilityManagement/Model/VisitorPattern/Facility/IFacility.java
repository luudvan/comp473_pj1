package com.FacilityManagement.Model.VisitorPattern.Facility;

import com.FacilityManagement.Model.VisitorPattern.User.Usertype.IVisitor;

public interface IFacility {
    public void accept(IVisitor visitor);

    public FacilityDetail getFacilityDetail();
    public void setFacilityDetail(FacilityDetail details);

    public void setFacilityID(int facilityID);

    public int getFacilityID();


}
