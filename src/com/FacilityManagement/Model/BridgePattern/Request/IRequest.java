package com.FacilityManagement.Model.BridgePattern.Request;

import com.FacilityManagement.Model.BridgePattern.Schedule.ISchedule;
import com.FacilityManagement.Model.Technology.iTechnology;
import com.FacilityManagement.Model.VisitorPattern.User.User.IUser;

public interface IRequest {



    public int getRequestID() ;

    public void setRequestID(int requestID);

    public String getRequestDescription();

    public void setRequestDescription(String requestDescription) ;
    public ISchedule getSchedule();
    public void setSchedule(ISchedule schedule);

    public iTechnology getTechnology();

    public void setTechnology(iTechnology technology);

    public IUser getiUser();

    public void setiUser(IUser iUser) ;





}
