package com.FacilityManagement.Model.VisitorPattern.User;

import com.FacilityManagement.Model.VisitorPattern.User.User.IUser;

public interface IUserService {
    void clearAllUserData();
    void addNewUser(IUser user);
    IUser getUserInformation(int ID);
    void removeUser(int id);
}
