package com.FacilityManagement.Model.BridgePattern.Request;

import com.FacilityManagement.Model.BridgePattern.Schedule.ISchedule;
import com.FacilityManagement.Model.BridgePattern.Schedule.Schedule;
import com.FacilityManagement.Model.Technology.Technology;
import com.FacilityManagement.Model.Technology.iTechnology;
import com.FacilityManagement.Model.VisitorPattern.User.User.IUser;

public class MaintainenceRequest implements IRequest {
    private int RequestID;
    private String RequestDescription;
    private ISchedule schedule = new Schedule();
    private iTechnology technology = new Technology();
    private IUser iUser ;
    private int cost;

    /*//spring
    private int RequestID;
    private String RequestDescription;
    private ISchedule schedule ;
    private iTechnology technology;
    private IUser iUser;
    private int cost;*/



    @Override
    public int getRequestID() {
        return RequestID;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }


    @Override
    public void setRequestID(int requestID) {
        this.RequestID = requestID;
    }

    @Override
    public String getRequestDescription() {
        return RequestDescription;
    }

    @Override
    public void setRequestDescription(String requestDescription) {
        this.RequestDescription = requestDescription;
    }

    @Override
    public ISchedule getSchedule() {
        return schedule;
    }

    @Override
    public void setSchedule(ISchedule schedule) {
        this.schedule = schedule;
    }

    @Override
    public iTechnology getTechnology() {
        return technology;
    }

    @Override
    public void setTechnology(iTechnology technology) {
        this.technology = technology;
    }

    @Override
    public IUser getiUser() {
        return iUser;
    }

    @Override
    public void setiUser(IUser iUser) {
        this.iUser = iUser;
    }




}
