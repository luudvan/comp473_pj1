package com.FacilityManagement.View;



import com.FacilityManagement.Model.BridgePattern.Request.UserRequest;
import com.FacilityManagement.Model.BridgePattern.Schedule.ISchedule;
import com.FacilityManagement.Model.BridgePattern.Schedule.Schedule;
import com.FacilityManagement.Model.Technology.Technology;
import com.FacilityManagement.Model.Technology.iTechnology;
import com.FacilityManagement.Model.VisitorPattern.User.Usertype.Faculty;
import com.FacilityManagement.Model.VisitorPattern.User.User.IUser;
import com.FacilityManagement.Model.VisitorPattern.User.IUserService;
import com.FacilityManagement.Model.VisitorPattern.User.UserService;
import com.FacilityManagement.Model.VisitorPattern.User.User.User;
import com.FacilityManagement.Model.VisitorPattern.Facility.Facility;
import com.FacilityManagement.Model.VisitorPattern.Facility.FacilityService;
import com.FacilityManagement.Model.VisitorPattern.Facility.IFacilityService;
import com.FacilityManagement.Model.ObserverPattern.Subscriber.IObservable;
import com.FacilityManagement.Model.ObserverPattern.Subscriber.Observable;
import com.FacilityManagement.Model.Facade.Services.UseService;

import java.time.LocalDate;

public class UseRequestClient {

    private IFacilityService facilityService = new FacilityService();
    private UseService useService = new UseService() ;
    private IUserService userService = new UserService();

    private ISchedule schedule = new Schedule();
    private IUser user = new User();
    private Faculty faculty =new Faculty();
    private iTechnology technology = new Technology();
    private IObservable observable = new Observable();
    private UserRequest userRequest = new UserRequest();




    //spring
/*
    private IFacilityService facilityService ;
    private UseService useService ;
    private ISchedule schedule;
    private Faculty faculty ;
    private iTechnology technology;
    private clearallUseData clearallUseData;
    private AddRequest addRequest;
    private removeRequest removeRequest;
    private vacateFacility vacateFacility;
    private isInUseDuringInterval isInUseDuringInterval;

    public UseRequestClient(IFacilityService facilityService, UseService useService, ISchedule schedule, Faculty faculty, iTechnology technology, com.FacilityManagement.Model.VisitorPattern.Actions.clearallUseData clearallUseData, AddRequest addRequest, com.FacilityManagement.Model.VisitorPattern.Actions.removeRequest removeRequest, com.FacilityManagement.Model.VisitorPattern.Actions.vacateFacility vacateFacility, com.FacilityManagement.Model.VisitorPattern.Actions.isInUseDuringInterval isInUseDuringInterval) {
        this.facilityService = facilityService;
        this.useService = useService;
        this.schedule = schedule;
        this.faculty = faculty;
        this.technology = technology;
        this.clearallUseData = clearallUseData;
        this.addRequest = addRequest;
        this.removeRequest = removeRequest;
        this.vacateFacility = vacateFacility;
        this.isInUseDuringInterval = isInUseDuringInterval;
    }*/

    public void start(){
       /* System.out.println("\nUseClient: *************** Clearing All Data *************************");
        useService.clearAllUseData();

*/

        //---------------------------------------------------------------

        System.out.println("\nUseClient: *************** Instantiating UseRequest *************************");
        Facility fac = facilityService.getFacilityInformation("Law Center");
        user = userService.getUserInformation(3);
        fac.accept(faculty);
        schedule.setRoomNumber(20);
        schedule.setStartDate(LocalDate.of(2020,05,01) );
        schedule.setEndDate(LocalDate.of(2020,05,04));
        schedule.setObservable(observable);

        technology.addtechnology(20116,"PC Computer");
        technology.addtechnology(20134,"Projector");

        userRequest = faculty.use(technology,schedule,"Class");

        useService.AddRequest(userRequest);




      /*  System.out.println("\nUseClient: *************** Vacate a Facility in Use in Schedule (Shorten Time)  *************************");
        useService.VacateFacility(4);

        System.out.println("\nUseRequestClient: *************** Remove a Facility  from UseRequest and Schedule *************************");
        useService.RemoveRequest(4);
        */

    }
}