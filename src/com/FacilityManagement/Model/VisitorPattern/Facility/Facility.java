package com.FacilityManagement.Model.VisitorPattern.Facility;


import com.FacilityManagement.Model.VisitorPattern.User.Usertype.IVisitor;

public class Facility implements IFacility{



	private int facilityID;


	private FacilityDetail facilityDetail = new FacilityDetail();



	@Override
	public void accept(IVisitor visitor) {
		visitor.visitFacility(this);
	}



	public FacilityDetail getFacilityDetail() {
		return this.facilityDetail;
	}

	public void setFacilityDetail(FacilityDetail details) {
		this.facilityDetail = details;
	}

	public void setFacilityID(int facilityID) {
		this.facilityID = facilityID;
	}

	public int getFacilityID() {
		return this.facilityID;
	}

}
