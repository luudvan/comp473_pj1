package com.FacilityManagement.Model.BridgePattern.Schedule;

import com.FacilityManagement.Model.VisitorPattern.Facility.IFacility;
import com.FacilityManagement.Model.ObserverPattern.Subscriber.IObservable;

import java.time.LocalDate;


public interface ISchedule {
     int getFacilityID();
     IFacility getFacility();
     int getScheduleID();
     void setScheduleID(int ScheduleID);
     void setRoomNumber(int roomNumber);
     int getRoomNumber();
     void setStartDate(LocalDate startDate);
     LocalDate getStartDate();
     LocalDate getEndDate();
     void setEndDate(LocalDate endDate);
     String RequestType();
     void setRequestType(String requestType);
     void setFacility(IFacility fac);

     public IObservable getObservable();

     public void setObservable(IObservable observable);
}

