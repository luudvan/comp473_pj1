package com.FacilityManagement.Model.VisitorPattern.User.Usertype;

import com.FacilityManagement.Model.BridgePattern.Request.InspectionRequest;
import com.FacilityManagement.Model.BridgePattern.Schedule.ISchedule;
import com.FacilityManagement.Model.Technology.iTechnology;
import com.FacilityManagement.Model.VisitorPattern.Facility.Facility;
import com.FacilityManagement.Model.VisitorPattern.Facility.IFacility;
import com.FacilityManagement.Model.VisitorPattern.User.User.IUser;

public class Inspector implements IVisitor {

   private IUser user;
    private InspectionRequest inspectionRequest = new InspectionRequest();
    private IFacility facility = new Facility();

    public IUser getUser() {
        return user;
    }

    public void setUser(IUser user) {
        this.user = user;
    }

    public InspectionRequest inspection(iTechnology iTechnology, ISchedule schedule, String RequestDes) {
        inspectionRequest.setiUser(user);
        inspectionRequest.setRequestDescription(RequestDes);
        inspectionRequest.setTechnology(iTechnology);
        System.out.print(facility);
        schedule.setFacility(facility);
        inspectionRequest.setSchedule(schedule);
        return inspectionRequest;
    }




    public void visitFacility(IFacility facility) {
        this.facility = facility;


    }


}

