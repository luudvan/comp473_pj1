package com.FacilityManagement.Model.VisitorPattern.User;

import com.FacilityManagement.DAL.UserDAO;
import com.FacilityManagement.Model.VisitorPattern.User.User.IUser;

public class UserService implements IUserService{

    private UserDAO userDAO = new UserDAO();
    @Override
    public void clearAllUserData() {
        userDAO.removeAllData();
    }

    @Override
    public void addNewUser(IUser user) {
        userDAO.addNewUser(user);
    }

    @Override
    public IUser getUserInformation(int ID) {
        return userDAO.getInformationByuserID(ID);
    }

    @Override
    public void removeUser(int id) {
        userDAO.removeuser(id);
    }
}
