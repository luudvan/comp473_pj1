package com.FacilityManagement.Model.Facade.Services;

import com.FacilityManagement.DAL.ScheduleDAO;
import com.FacilityManagement.DAL.UseRequestDAO;
import com.FacilityManagement.DAL.UserDAO;
import com.FacilityManagement.Model.BridgePattern.Request.IRequest;
import com.FacilityManagement.Model.BridgePattern.Request.UserRequest;
import com.FacilityManagement.Model.BridgePattern.Schedule.ISchedule;
import com.FacilityManagement.Model.VisitorPattern.User.User.IUser;
import com.FacilityManagement.Model.VisitorPattern.User.User.User;

import java.util.Scanner;

public class UseService {


    private UseRequestDAO useRequestDAO = new UseRequestDAO();
    private UserDAO userDAO = new UserDAO();
    private IUser user = new User();
    private Scanner scanner = new Scanner(System.in);
    private ScheduleDAO scheduleDAO = new ScheduleDAO();


    /*private UserRequest userRequest;
    private UseRequestDAO useRequestDAO;

 //spring
    public UserRequest getUserRequest() {
        return userRequest;
    }

    public void setUserRequest(UserRequest userRequest) {
        this.userRequest = userRequest;
    }

    public UseRequestDAO getUseRequestDAO() {
        return useRequestDAO;
    }

    public void setUseRequestDAO(UseRequestDAO useRequestDAO) {
        this.useRequestDAO = useRequestDAO;
    }

    //spring*/

    public void AddRequest(UserRequest userRequest) {
        user = userDAO.getInformationByuserID(userRequest.getiUser().getUserID());
        if (!(useRequestDAO.isInUseDuringInterval(userRequest))) {
            userRequest.getSchedule().setRequestType("Use");
            useRequestDAO.AddUseRequest(userRequest);
            System.out.println("Successfully added your request");
        }
        else {
            Subscribe();
        }
    }


    public void isInUseDuringInterval(UserRequest userRequest) {
        useRequestDAO.isInUseDuringInterval(userRequest);
    }


    public void RemoveRequest(int ID) {
        UserRequest userRequest = useRequestDAO.getInfobyID(ID);
        useRequestDAO.removeUseRequest(userRequest);
    }


    public void VacateFacility(int ID) {
        UserRequest userRequest = useRequestDAO.getInfobyID(ID);
        useRequestDAO.VacateInsFacility(userRequest);
    }


    public void clearAllUseData() {
        useRequestDAO.removeAllData();
    }


    public IRequest getInfobyID(int ID) {
        return useRequestDAO.getInfobyID(ID);
    }


    private void Subscribe() {
        System.out.println("Sorry this room is not available in this time, " +
                "would you like to get notified when its available in this time? Y/N ");
        String answer = scanner.nextLine();
        if (answer.equals("Y")) {
            ISchedule schedule = scheduleDAO.getInfobyID(useRequestDAO.getID());
            scheduleDAO.addSubscriber(schedule, user);


            System.out.println("You will be notified when the room is available in the selected time via your choice of service");
        } else if (answer.equals("N")) {
            System.out.println("Please choose another room");

        } else {
            System.out.println("Wrong input, please try again");
            Subscribe();
        }
    }
}
