package com.FacilityManagement.Model.ObserverPattern.Subscriber;

import com.FacilityManagement.Model.VisitorPattern.User.User.IUser;

public interface IObservable {
    void addObserver(IUser user);
    void removeObserver(IUser user);
    void notifyObserver();
    public String listofobserverid();
   /* public boolean isAvailable();
    public void setAvailable(boolean available);*/
}
