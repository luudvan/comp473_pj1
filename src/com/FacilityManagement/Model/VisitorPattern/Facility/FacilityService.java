package com.FacilityManagement.Model.VisitorPattern.Facility;

import com.FacilityManagement.DAL.FacilityDAO;



import java.util.List;

public class FacilityService implements IFacilityService {

	private FacilityDAO facDAO = new FacilityDAO();


	public void setFacDAO(FacilityDAO facDAO) {
		this.facDAO = facDAO;
	}


	public void clearAllFacilityData() {
		facDAO.removeAllData();
	}

	// Adding a new facility to the database
	public void addNewFacility(Facility facility) {
		facDAO.addNewFacility(facility);
	}

	public Facility getFacilityInformation(String facName) {
		return facDAO.getFacilityInformationByFacilityName(facName);
	}

	public Facility getFacilityInformationbyID(int ID) {
		return facDAO.getFacilityInformationByFacilityID(ID);
	}

	public void removeFacility(int id) {
		facDAO.removeFacility(id);
	}

	public void addFacilityDetail(int ID, int PhoneNumber) {
		facDAO.addFacilityDetail(ID, PhoneNumber);
	}

	public List<Facility> listFacilities() {
		return facDAO.listFacilities();
	}
}

