package com.FacilityManagement.View;


//import org.springframework.context.ApplicationContext;
//import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Client {

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		try {
			FacilityClient facilityClient = new FacilityClient();
			MaintainenceClient maintenanceClient = new MaintainenceClient();
			UseRequestClient userequestClient = new UseRequestClient();
			InspectionClient inspectionClient = new InspectionClient();
			UserClient userClient = new UserClient();


			//Spring Implementation
			//ApplicationContext context = new ClassPathXmlApplicationContext("META-INF/app-context.xml");
			//MaintainenceClient maintenanceClient=(MaintainenceClient)context.getBean("maintainenceClient");
			// UseRequestClient useClient =(UseRequestClient) context.getBean("useClient") ;
			//InspectionClient inspectionClient = (InspectionClient) context.getBean("inspectionClient");
			//FacilityClient facilityClient = (FacilityClient) context.getBean("facilityClient");


			//maintenanceClient.start();
			//userequestClient.start();
			//inspectionClient.start();
			//facilityClient.start();
			userClient.start();

		} catch (Exception e) {
			System.out.println(e);
		}



	}

}
