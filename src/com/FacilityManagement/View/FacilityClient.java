package com.FacilityManagement.View;

import com.FacilityManagement.Model.VisitorPattern.Facility.Facility;
import com.FacilityManagement.Model.VisitorPattern.Facility.FacilityDetail;
import com.FacilityManagement.Model.VisitorPattern.Facility.FacilityService;
import com.FacilityManagement.Model.VisitorPattern.Facility.IFacilityService;

public class FacilityClient {

	private IFacilityService facilityService = new FacilityService();
	private Facility fac  = new Facility();
	private FacilityDetail detail = new FacilityDetail();

	/*//spring
	private IFacilityService facilityService;
	private Facility fac  ;
	private FacilityDetail detail ;

	public FacilityClient(IFacilityService facilityService, Facility fac, FacilityDetail detail) {
		this.facilityService = facilityService;
		this.fac = fac;
		this.detail = detail;
	}*/

	public void start()  {



		//---------------------------------------------------------------
		System.out.println("\nFacilityClient: *************** Instantiating a facility and its details *************************");



		detail.setName("Libary");
		detail.setNumberOfRooms(10);
		detail.setPhoneNumber(5553);
		fac.setFacilityDetail(detail);
		facilityService.addNewFacility(fac);


		/*detail.setName("Law Center2");
		detail.setNumberOfRooms(10);
		detail.setPhoneNumber(123456);
		fac.setFacilityDetail(detail);
		facilityService.addNewFacility(fac);


		detail.setName("Student Center2");
		detail.setNumberOfRooms(100);
		detail.setPhoneNumber(111111);
		fac.setFacilityDetail(detail);
		facilityService.addNewFacility(fac);*/


		/*System.out.println("FacilityClient: *************** Facility is inserted in Facility Database *************************");
		fac = facilityService.getFacilityInformationbyID(11);
		System.out.println(fac);*/
		//---------------------------------------------------------------
		/*System.out.println("FacilityClient: *************** Trying to get information about this facility in the database ***************");
		Facility searchedFacility = facilityService.getFacilityInformation("IT Center");
		System.out.println("\nFacilityClient: *************** Here is searched facility information *************************");
		System.out.println("\n\tFacility ID:   \t\t" + searchedFacility.getFacilityID());
		FacilityDetail facilityDet = searchedFacility.getFacilityDetail();
		System.out.println("\tInfo About Facility:  \t" + facilityDet.getName() +
				"\n\t\t\t\t Number of Rooms:" + facilityDet.getNumberOfRooms());
		if (facilityDet.getPhoneNumber() != 0) {
			System.out.print("\t\t\t\t Phone Number: " + facilityDet.getPhoneNumber() +
					"\n\t\t\t\t" + "\n");
		} else {
			System.out.print("\t\t\t\t Phone Number: unlisted" +
					"\n\t\t\t\t" + "\n");
		}
*/

		//---------------------------------------------------------------
	/*	facilityService.addFacilityDetail(fac.getFacilityID(), 3120136);
		Facility updatedFacility = facilityService.getFacilityInformation(fac.getFacilityDetail().getName());
		FacilityDetail facilityNewDet = updatedFacility.getFacilityDetail();

		System.out.println("\nFacilityClient: *************** Here is the updated facility information *************************");
		System.out.println("\n\tFacility ID:   \t\t" + updatedFacility.getFacilityID());
		System.out.println("\tInfo About Facility:  \t" + facilityNewDet.getName() +
				"\n\t\t\t\t Number of Rooms: " + facilityNewDet.getNumberOfRooms());
		if (facilityNewDet.getPhoneNumber() != 0) {
			System.out.print("\t\t\t\t Phone Number: " + facilityNewDet.getPhoneNumber() +
					"\n\t\t\t\t" + "\n");
		} else {
			System.out.print("\t\t\t\t Phone Number: unlisted" +
					"\n\t\t\t\t" + "\n");
		}*/


		//---------------------------------------------------------------
		/*System.out.println("\nFacilityClient: *************** Remove a facility from the database *************************");
		facilityService.removeFacility(fac.getFacilityID());
		System.out.println("************ Facility Removed ************");*/

		/*System.out.println("\nFacilityClient: *************** An updated list of all the facilities *************************");
		List<Facility> listOfFacilities = facilityService.listFacilities();
		for (Facility fact : listOfFacilities) {
			FacilityDetail facDet = fact.getFacilityDetail();
			System.out.println("\n\t" + facDet.getName() + " ID: " + fac.getFacilityID());
		}
*/
		//---------------------------------------------------------------
		/*System.out.println("\nFacilityClient: *************** Request available capacity of a facility *************************");
		//uses sample data
		//int roomsAvail = facService.requestAvailableCapacity(fac2);
		//System.out.println("There are " + roomsAvail + " rooms currently available at Facility #" + fac2.getFacilityID() + ".");
*/
		//---------------------------------------------------------------
	/*	System.out.println("\nFacilityClient: *************** Remove Facility *************************");
		facilityService.removeFacility(fac.getFacilityID());*/
	}
}
