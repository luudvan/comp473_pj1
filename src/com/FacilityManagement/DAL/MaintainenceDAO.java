package com.FacilityManagement.DAL;


import com.FacilityManagement.Model.BridgePattern.Request.MaintainenceRequest;
import com.FacilityManagement.Model.BridgePattern.Schedule.ISchedule;
import com.FacilityManagement.Model.BridgePattern.Schedule.Schedule;
import com.FacilityManagement.Model.Technology.Technology;
import com.FacilityManagement.Model.Technology.iTechnology;
import com.FacilityManagement.Model.VisitorPattern.User.User.IUser;
import com.FacilityManagement.Model.VisitorPattern.User.User.User;

import java.sql.*;
import java.util.*;

public class MaintainenceDAO {

        private ScheduleDAO scheduleDAO  = new ScheduleDAO(); //Spring
        private UserDAO userDAO = new UserDAO();
        private int ID;

        public int getID() {
            return ID;
        }

        public void setScheduleDAO(ScheduleDAO scheduleDAO) {
            this.scheduleDAO = scheduleDAO;
        }

        public void removeAllData() {
            try {
                Statement st = DB_Helper.getConnection().createStatement();
                String removeUseRequestQuery = "delete from MaintainenceRequest";
                String removeUseSchedule = "delete from schedule where requesttype = " + "'" + "Maintainence" + "'";
                st.execute(removeUseRequestQuery);
                st.execute(removeUseSchedule);

                System.out.println("MaintainenceRequest DAO: *************** Query " + removeUseRequestQuery + "\n");
                st.close();
            } catch (SQLException se) {
                System.err.println("MaintainenceRequestDAO: Threw a SQLException removing the MaintainenceRequest object from MaintainanceRequest table.");
                System.err.println(se.getMessage());
                se.printStackTrace();
            }
        }

        public void removeMaintainenceRequest(MaintainenceRequest MainRequest) {
            try {
                Statement st = DB_Helper.getConnection().createStatement();
                String removeMaintRequestQuery = "delete from MaintainenceRequest where scheduleID = '" + MainRequest.getSchedule().getScheduleID() + "'";
                st.execute(removeMaintRequestQuery);
                String removeScheduleQuery = "delete from schedule where scheduleID = '" + MainRequest.getSchedule().getScheduleID() + "'";
                st.execute(removeScheduleQuery);
                System.out.println("MaintainenceRequest DAO: *************** Query " + removeMaintRequestQuery + "\n");
                System.out.println("Schedule DAO: *************** Query " + removeScheduleQuery + "\n");
                st.close();
            } catch (SQLException se) {
                System.err.println("MaintainenceRequestDAO: Threw a SQLException removing the Maintainence object from MaintainenceRequest table.");
                System.err.println(se.getMessage());
                se.printStackTrace();
            }

        }

        public void AddmaintRequest(MaintainenceRequest MainRequest) {


                Connection con = DB_Helper.getConnection();
                PreparedStatement addMain = null;
                Random rand = new Random(); //Spring ?

                try {
                    //InsetMaintainenceService Object
                    scheduleDAO.AssignFacilityToUse(MainRequest.getSchedule());
                    //System.out.println(MainRequest.getMaintainence().getScheduleID());

                    //Insert the Mainaintence object
                    String addReq = "INSERT INTO MaintainenceRequest(MaintainenceRequestID, FacilityID, ScheduleID, RoomNumber,MaintainenceDescription ,Cost ,TechType  ,TechCodes  ,userid ) VALUES(?, ?, ?, ?,?,?,?,?,?)";
                    addMain = con.prepareStatement(addReq);
                    addMain.setInt(1, MainRequest.getRequestID());
                    addMain.setInt(2, MainRequest.getSchedule().getFacilityID());
                    addMain.setInt(3, MainRequest.getSchedule().getScheduleID());
                    addMain.setInt(4, MainRequest.getSchedule().getRoomNumber());
                    addMain.setString(5, MainRequest.getRequestDescription());
                    addMain.setInt(6,MainRequest.getCost());
                    addMain.setString(7,MainRequest.getTechnology().ListoftechType());
                    addMain.setString(8, MainRequest.getTechnology().ListoftechCode());
                    addMain.setInt(9,MainRequest.getiUser().getUserID());


                    addMain.executeUpdate();


                } catch (SQLException ex) {
                    System.out.println(ex);

                } finally {

                    try {
                        if (addMain != null) {
                            addMain.close();
                        }
                        if (con != null) {
                            con.close();
                        }

                    } catch (SQLException ex) {
                        System.err.println("MaintainenceRequestDAO: Threw a SQLException saving the MainRequest object.");
                        System.err.println(ex.getMessage());
                    }

                }


            }



    public boolean isInUseDuringInterval(MaintainenceRequest ins) {
        if (!scheduleDAO.isInUseDuringInterval(ins.getSchedule())){
            return false;
        }
        ID = scheduleDAO.getID();
        return true;

    }
    public void VacateInsFacility(MaintainenceRequest ins) {
        scheduleDAO.VacateFacility(ins.getSchedule());
    }

    public MaintainenceRequest getInfobyID(int ID) {
        try {

            Statement st = DB_Helper.getConnection().createStatement();
            String selectDetailQuery = "SELECT *  FROM MaintainenceRequest,Schedule WHERE Schedule.ScheduleID= '" + ID + "' AND MaintainenceRequest.ScheduleID = Schedule.ScheduleID ";
            ResultSet detRS = st.executeQuery(selectDetailQuery);

            return achievemaintanience(detRS, selectDetailQuery);
        }

        catch (SQLException se) {
            System.err.println("FacilityDAO: Threw a SQLException retrieving the Facility object.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }

        return null;
    }

    private MaintainenceRequest achievemaintanience(ResultSet detRS, String selectDetailQuery) {
        try {
            MaintainenceRequest maintainenceRequest = new MaintainenceRequest();
            FacilityDAO facilityDAO = new FacilityDAO();
            ISchedule schedule = new Schedule();
            iTechnology technology = new Technology();
            IUser user = new User();




            System.out.println("MaintenanceDAO: *************** Query " + selectDetailQuery + "\n");

            while ( detRS.next() ) {
                maintainenceRequest.setRequestID(detRS.getInt("MaintainenceRequestID"));
                maintainenceRequest.setCost(detRS.getInt("cost"));
                maintainenceRequest.setRequestDescription(detRS.getString("maintainencedescription"));

                String code = detRS.getString("TechCodes");
                String type = detRS.getString("TechType");
                technology.setTechnologytype( new ArrayList<String>(Arrays.asList(type.split(","))));
                List<String> listcode = new ArrayList<String>(Arrays.asList(code.split(" ,")));
                ArrayList<Integer> codereal = new ArrayList<Integer>();
                for(String s : listcode) codereal.add(Integer.valueOf(s));
                technology.setTechnologycode(codereal);

                user = userDAO.getInformationByuserID(detRS.getInt("userID"));
                schedule.setRequestType("requesttype");
                schedule.setFacility(facilityDAO.getFacilityInformationByFacilityID(detRS.getInt("facilityid")));
                schedule.setScheduleID(detRS.getInt("scheduleid"));
                schedule.setRoomNumber(detRS.getInt("roomnumber"));
                schedule.setStartDate(detRS.getDate("startdate").toLocalDate());
                schedule.setEndDate(detRS.getDate("enddate").toLocalDate());
            }

            maintainenceRequest.setSchedule(schedule);
            maintainenceRequest.setiUser(user);
            maintainenceRequest.setTechnology(technology);

            //close to manage resources
            detRS.close();

            return maintainenceRequest;
        } catch (SQLException se) {
            System.err.println("maintenanceDAO: Threw a SQLException retrieving the request object.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }

        return null;

    }


}


