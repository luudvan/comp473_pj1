package com.FacilityManagement.View;

import com.FacilityManagement.Model.VisitorPattern.User.*;
import com.FacilityManagement.Model.VisitorPattern.User.User.IUser;
import com.FacilityManagement.Model.VisitorPattern.User.User.User;
import com.FacilityManagement.Model.ObserverPattern.ObserverNotify.IObserver;
import com.FacilityManagement.Model.ObserverPattern.ObserverNotify.viaSMS;

public class UserClient {

    IUserService userService = new UserService();
    private IObserver observer = new viaSMS();
    private IUser user;




    public void start() {
        System.out.println("\nUserClient: *************** Instantiating UserRequest *************************");
        user = new User();
        user.setUsername("Albert");
        user.setPhonenumber("8722032998");
        user.setDepartment("ITRS");
        user.setUsertype("Inspector");
        user.setObserver(observer);
        userService.addNewUser(user);

        user =new User();
        user.setUsername("Baboon");
        user.setPhonenumber("7738074658");
        user.setDepartment("Armpit Research");
        user.setUsertype("Technician");
        user.setObserver(observer);
        userService.addNewUser(user);

        user = new User();
        user.setUsername("Marian");
        user.setPhonenumber("2173208198");
        user.setDepartment("Engineering");
        user.setUsertype("Inspector");
        user.setObserver(observer);
        userService.addNewUser(user);

        //user = userService.getUserInformation(2);
        //System.out.println(user);

      /*  System.out.println("\nUserClient: *************** Remove a user from the database *************************");
        userService.removeUser(1);
        System.out.println("************ User Removed ************");*/
    }

    }
