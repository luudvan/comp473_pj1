package com.FacilityManagement.Model.VisitorPattern.User.Usertype;

import com.FacilityManagement.Model.BridgePattern.Request.MaintainenceRequest;
import com.FacilityManagement.Model.BridgePattern.Schedule.ISchedule;
import com.FacilityManagement.Model.Technology.iTechnology;
import com.FacilityManagement.Model.VisitorPattern.Facility.Facility;
import com.FacilityManagement.Model.VisitorPattern.Facility.IFacility;
import com.FacilityManagement.Model.VisitorPattern.User.User.IUser;

public class Technician implements IVisitor  {

    private IUser user;

    private MaintainenceRequest maintainenceRequest = new MaintainenceRequest();
    private IFacility facility = new Facility();

    public IUser getUser() {
        return user;
    }

    public void setUser(IUser user) {
        this.user = user;
    }

    public MaintainenceRequest maintain(iTechnology iTechnology, ISchedule schedule, String RequestDes, int cost) {
        maintainenceRequest.setiUser(user);
        maintainenceRequest.setRequestDescription(RequestDes);
        maintainenceRequest.setTechnology(iTechnology);
        schedule.setFacility(facility);
        maintainenceRequest.setSchedule(schedule);
        maintainenceRequest.setCost(cost);
        return maintainenceRequest;
    }

    public void visitFacility(IFacility facility) {
        this.facility = facility;
    }



}
