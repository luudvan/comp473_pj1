package com.FacilityManagement.Model.Facade.Services;

import com.FacilityManagement.DAL.MaintainenceDAO;
import com.FacilityManagement.DAL.ScheduleDAO;
import com.FacilityManagement.DAL.UserDAO;
import com.FacilityManagement.Model.BridgePattern.Request.IRequest;
import com.FacilityManagement.Model.BridgePattern.Request.MaintainenceRequest;
import com.FacilityManagement.Model.BridgePattern.Schedule.ISchedule;
import com.FacilityManagement.Model.VisitorPattern.User.User.IUser;
import com.FacilityManagement.Model.VisitorPattern.User.User.User;

import java.util.Scanner;

public class MaintainenceService {

    private MaintainenceRequest maintainenceRequest = new MaintainenceRequest();
    private MaintainenceDAO maintainenceDAO = new MaintainenceDAO();
    private UserDAO userDAO = new UserDAO();
    private IUser user = new User();
    private Scanner scanner =  new Scanner(System.in);
    private ScheduleDAO scheduleDAO = new ScheduleDAO();

   /* private MaintainenceRequest maintainenceRequest;
    private MaintainenceDAO maintainenceDAO;

    //spring

    public MaintainenceRequest getMaintainenceRequest() {
        return maintainenceRequest;
    }

    public void setMaintainenceRequest(MaintainenceRequest maintainenceRequest) {
        this.maintainenceRequest = maintainenceRequest;
    }

    public MaintainenceDAO getMaintainenceDAO() {
        return maintainenceDAO;
    }

    public void setMaintainenceDAO(MaintainenceDAO maintainenceDAO) {
        this.maintainenceDAO = maintainenceDAO;
    }

    //spring*/


    public void AddRequest(MaintainenceRequest maintainenceRequest) {

        user = userDAO.getInformationByuserID(maintainenceRequest.getiUser().getUserID());
        if (!(maintainenceDAO.isInUseDuringInterval(maintainenceRequest))) {
            maintainenceRequest.getSchedule().setRequestType("Maintainence");
            maintainenceDAO.AddmaintRequest(maintainenceRequest);
            System.out.println("Successfully added your request");
        }
        else {
            Subscribe();
        }
    }


    public void isInUseDuringInterval(MaintainenceRequest maintainenceRequest) {
        maintainenceDAO.isInUseDuringInterval(maintainenceRequest);
    }


    public void RemoveRequest(int ID) {
        maintainenceRequest=maintainenceDAO.getInfobyID(ID);
        maintainenceDAO.removeMaintainenceRequest(maintainenceRequest);
    }


    public void VacateFacility(int ID) {
        maintainenceRequest=maintainenceDAO.getInfobyID(ID);
        maintainenceDAO.VacateInsFacility(maintainenceRequest);
    }


    public void clearAllUseData() {
        maintainenceDAO.removeAllData();
    }


    public IRequest getInfobyID(int ID) {
        maintainenceRequest =maintainenceDAO.getInfobyID(ID);
    return maintainenceRequest;
    }





    private void Subscribe(){
        System.out.println("Sorry this room is not available in this time, " +
                "would you like to get notified when its available in this time? Y/N ");
        String answer = scanner.nextLine();
        if(answer.equals("Y")){
            ISchedule schedule = scheduleDAO.getInfobyID(maintainenceDAO.getID());
            scheduleDAO.addSubscriber(schedule,user);


            System.out.println("You will be notified when the room is available in the selected time via your choice of service");
        }
        else if(answer.equals("N")){
            System.out.println("Please choose another room");

        }
        else {
            System.out.println("Wrong input, please try again");
            Subscribe();
        }


    }
}
