package com.FacilityManagement.Model.ObserverPattern.ObserverNotify;

public interface IObserver {
     void update(String information);
    String getType();

}
