package com.FacilityManagement.Model.VisitorPattern.Facility;

import java.util.List;

public interface IFacilityService {
     void clearAllFacilityData();
     void addNewFacility(Facility facility);
     Facility getFacilityInformation(String facName);
     void removeFacility(int id);
     public Facility getFacilityInformationbyID(int ID) ;
          void addFacilityDetail(int ID, int PhoneNumber);
     List<Facility> listFacilities();


}
