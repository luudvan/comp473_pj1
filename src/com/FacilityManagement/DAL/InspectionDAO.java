package com.FacilityManagement.DAL;

import com.FacilityManagement.Model.BridgePattern.Request.InspectionRequest;
import com.FacilityManagement.Model.BridgePattern.Schedule.ISchedule;
import com.FacilityManagement.Model.BridgePattern.Schedule.Schedule;
import com.FacilityManagement.Model.Technology.Technology;
import com.FacilityManagement.Model.Technology.iTechnology;
import com.FacilityManagement.Model.VisitorPattern.User.User.IUser;
import com.FacilityManagement.Model.VisitorPattern.User.User.User;
import com.FacilityManagement.Model.ObserverPattern.Subscriber.IObservable;
import com.FacilityManagement.Model.ObserverPattern.Subscriber.Observable;

import java.sql.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class InspectionDAO {
    private ScheduleDAO scheduleDAO = new ScheduleDAO(); //Spring
    private int ID;

    public int getID() {
        return ID;
    }

    public void setScheduleDAO(ScheduleDAO scheduleDAO) {
        this.scheduleDAO = scheduleDAO;
    }

    public void removeAllData() {
        try {
            Statement st = DB_Helper.getConnection().createStatement();
            String removeInsRequestQuery = "delete from InspectionRequest";
            String removeInsSchedule = "delete from schedule where requesttype = " + "'" + "Inspection" + "'";
            st.execute(removeInsRequestQuery);
            st.execute(removeInsSchedule);

            System.out.println("InspectionRequestDAO: *************** Query " + removeInsRequestQuery + "\n");
            st.close();
        } catch (SQLException se) {
            System.err.println("InspectionRequestDAO: Threw a SQLException removing the InspectionRequest object from InspectionRequest table.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }
    }

    public void removeInspectionRequest(InspectionRequest InsRequest) {
        try {
            Statement st = DB_Helper.getConnection().createStatement();
            String removeInsRequestQuery = "delete from InspectionRequest where scheduleID = '" + InsRequest.getSchedule().getScheduleID() + "'";
            st.execute(removeInsRequestQuery);
            String removeScheduleQuery = "delete from schedule where scheduleID = '" + InsRequest.getSchedule().getScheduleID() + "'";
            st.execute(removeScheduleQuery);
            System.out.println("InspectionRequest DAO: *************** Query " + removeInsRequestQuery + "\n");
            System.out.println("Schedule DAO: *************** Query " + removeScheduleQuery + "\n");
            st.close();
        } catch (SQLException se) {
            System.err.println("InspectionRequestDAO: Threw a SQLException removing the Inspection object from InspectionRequest table.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }

    }

    public void AddInsRequest(InspectionRequest InsRequest) {

            Connection con = DB_Helper.getConnection();
            PreparedStatement addIns = null;
            Random rand = new Random();

            try {
                //InsetMaintainenceService Object
                scheduleDAO.AssignFacilityToUse(InsRequest.getSchedule());
                //System.out.println(InsRequest.getInspection().getScheduleID());

                //Insert the inspection object
                String addReq = "INSERT INTO InspectionRequest(InspectionRequestID, FacilityID, ScheduleID, RoomNumber,InspectionDescription ,Equipmenttype  ,Equipmentcode ,userID ) VALUES(?, ?, ?, ?,?,?,?,?)";
                addIns = con.prepareStatement(addReq);
                addIns.setInt(1, InsRequest.getRequestID());
                addIns.setInt(2, InsRequest.getSchedule().getFacilityID());
                addIns.setInt(3, InsRequest.getSchedule().getScheduleID());
                addIns.setInt(4, InsRequest.getSchedule().getRoomNumber());
                addIns.setString(5, InsRequest.getRequestDescription());
                addIns.setString(6,InsRequest.getTechnology().ListoftechType());
                addIns.setString(7,InsRequest.getTechnology().ListoftechCode());
                addIns.setInt(8, InsRequest.getiUser().getUserID());





                addIns.executeUpdate();


            } catch (SQLException ex) {
                System.out.println(ex);

            } finally {

                try {
                    if (addIns != null) {
                        addIns.close();
                    }
                    if (con != null) {
                        con.close();
                    }

                } catch (SQLException ex) {
                    System.err.println("FacilityDAO: Threw a SQLException saving the InsRequest object.");
                    System.err.println(ex.getMessage());
                }

            }


        }


    public boolean isInUseDuringInterval(InspectionRequest ins) {
        if (!scheduleDAO.isInUseDuringInterval(ins.getSchedule())){
            return false;
        }
        ID = scheduleDAO.getID();
        return true;

    }



    public void VacateInsFacility(InspectionRequest ins) {
        scheduleDAO.VacateFacility(ins.getSchedule());
    }

    public InspectionRequest getInfobyID(int ID) {
        try {

            Statement st = DB_Helper.getConnection().createStatement();
            String selectDetailQuery = "SELECT *  FROM inspectionrequest,Schedule WHERE Schedule.ScheduleID= '" + ID + "' AND inspectionrequest.ScheduleID = Schedule.ScheduleID ";
            ResultSet detRS = st.executeQuery(selectDetailQuery);

            return achieveinspection(detRS, selectDetailQuery);
        }

        catch (SQLException se) {
            System.err.println("FacilityDAO: Threw a SQLException retrieving the Facility object.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }

        return null;
    }

    private InspectionRequest achieveinspection(ResultSet detRS, String selectDetailQuery) {
        try {
            InspectionRequest inspectionRequest = new InspectionRequest();
            FacilityDAO facilityDAO = new FacilityDAO();
            ISchedule schedule = new Schedule();
            iTechnology technology = new Technology();
            IUser user = new User();
            UserDAO userDAO = new UserDAO();
            IObservable observable = new Observable();




            System.out.println("inspectionDAO: *************** Query " + selectDetailQuery + "\n");

            while ( detRS.next() ) {
                inspectionRequest.setRequestID(detRS.getInt("inspectionrequestid"));
                inspectionRequest.setRequestDescription(detRS.getString("inspectiondescription"));

                String code = detRS.getString("equipmentcode");
                String type = detRS.getString("equipmenttype");
                technology.setTechnologytype( new ArrayList<String>(Arrays.asList(type.split(","))));
                List<String> listcode = new ArrayList<String>(Arrays.asList(code.split(" ,")));
                ArrayList<Integer> codereal = new ArrayList<Integer>();
                for(String s : listcode) codereal.add(Integer.valueOf(s));
                technology.setTechnologycode(codereal);

                inspectionRequest.setiUser(userDAO.getInformationByuserID(detRS.getInt("UserID")));

                schedule.setRequestType("requesttype");
                schedule.setFacility(facilityDAO.getFacilityInformationByFacilityID(detRS.getInt("facilityid")));
                schedule.setScheduleID(detRS.getInt("scheduleid"));
                schedule.setRoomNumber(detRS.getInt("roomnumber"));
                schedule.setStartDate(detRS.getDate("startdate").toLocalDate());
                schedule.setEndDate(detRS.getDate("enddate").toLocalDate());
                if (!(detRS.getString("UserIDList").equals( "none"))) {
                    String subscriber = detRS.getString("useridlist");
                    List<String> userid = new ArrayList<String>(Arrays.asList(subscriber.split(",")));
                    ArrayList<Integer> useridint = new ArrayList<Integer>();
                    for(String s : userid) useridint.add(Integer.valueOf(s));
                    for (int m : useridint) observable.addObserver(userDAO.getInformationByuserID(m));

                }
                schedule.setObservable(observable);
            }

            inspectionRequest.setSchedule(schedule);
            inspectionRequest.setiUser(user);
            inspectionRequest.setTechnology(technology);

            //close to manage resources
            detRS.close();

            return inspectionRequest;
        } catch (SQLException se) {
            System.err.println("inspectionDAO: Threw a SQLException retrieving the request object.");
            System.err.println(se.getMessage());
            se.printStackTrace();
        }

        return null;

    }

}
