package com.FacilityManagement.View;

import com.FacilityManagement.Model.BridgePattern.Request.MaintainenceRequest;
import com.FacilityManagement.Model.BridgePattern.Schedule.ISchedule;
import com.FacilityManagement.Model.BridgePattern.Schedule.Schedule;
import com.FacilityManagement.Model.Technology.Technology;
import com.FacilityManagement.Model.Technology.iTechnology;
import com.FacilityManagement.Model.VisitorPattern.User.User.IUser;
import com.FacilityManagement.Model.VisitorPattern.User.IUserService;
import com.FacilityManagement.Model.VisitorPattern.User.Usertype.Technician;
import com.FacilityManagement.Model.VisitorPattern.User.UserService;
import com.FacilityManagement.Model.VisitorPattern.User.User.User;
import com.FacilityManagement.Model.VisitorPattern.Facility.FacilityService;
import com.FacilityManagement.Model.VisitorPattern.Facility.IFacility;
import com.FacilityManagement.Model.VisitorPattern.Facility.IFacilityService;
import com.FacilityManagement.Model.ObserverPattern.Subscriber.IObservable;
import com.FacilityManagement.Model.ObserverPattern.Subscriber.Observable;
import com.FacilityManagement.Model.Facade.Services.MaintainenceService;

import java.time.LocalDate;

public class MaintainenceClient {

    private IFacilityService facilityService = new FacilityService();
    private MaintainenceService maintainenceService =new MaintainenceService();
    private MaintainenceRequest maintainenceRequest = new MaintainenceRequest();
    private IUserService userService = new UserService();

    private ISchedule schedule = new Schedule();
    private IUser user = new User();
    private iTechnology technology = new Technology();
    private Technician technician = new Technician();
    private IObservable observable = new Observable();



    //spring
   /* private IFacilityService facilityService ;
    private MaintainenceService maintainenceService ;
    private ISchedule schedule;
    private Technician technician ;
    private iTechnology technology;
    private clearallUseData clearallUseData;
    private AddRequest addRequest;
    private removeRequest removeRequest;
    private vacateFacility vacateFacility;
    private isInUseDuringInterval isInUseDuringInterval;
*/
   /* public MaintainenceClient(IFacilityService facilityService, MaintainenceService maintainenceService, ISchedule schedule, Technician technician, iTechnology technology, com.FacilityManagement.Model.VisitorPattern.Actions.clearallUseData clearallUseData, AddRequest addRequest, com.FacilityManagement.Model.VisitorPattern.Actions.removeRequest removeRequest, com.FacilityManagement.Model.VisitorPattern.Actions.vacateFacility vacateFacility, com.FacilityManagement.Model.VisitorPattern.Actions.isInUseDuringInterval isInUseDuringInterval) {
        this.facilityService = facilityService;
        this.maintainenceService = maintainenceService;
        this.schedule = schedule;
        this.technician = technician;
        this.technology = technology;
        this.clearallUseData = clearallUseData;
        this.addRequest = addRequest;
        this.removeRequest = removeRequest;
        this.vacateFacility = vacateFacility;
        this.isInUseDuringInterval = isInUseDuringInterval;
    }*/

    public void start(){
        System.out.println("\nMaintenanceClient: *************** Clearing All Data *************************");
        maintainenceService.clearAllUseData();

       // ---------------------------------------------------------------

   System.out.println("\nMaintenanceClient: *************** Instantiating MaintenanceRequest *************************");
        IFacility facility = facilityService.getFacilityInformation("Law Center");
        user = userService.getUserInformation(4);
        technician.setUser(user);
        facility.accept(technician);
        schedule.setRoomNumber(20);
        schedule.setStartDate(LocalDate.of(2020,05,01) );
        schedule.setEndDate(LocalDate.of(2020,05,04));
        schedule.setObservable(observable);


        technology.addtechnology(2222,"Air conditioner");
        maintainenceRequest = technician.maintain(technology,schedule,"Repair",300);
        maintainenceService.AddRequest(maintainenceRequest);


        System.out.println("\nMaintenanceClient: *************** Vacate a Facility in Maintenance in Schedule (Shorten Time)  *************************");
         maintainenceService.VacateFacility(3); //scheduleid


 System.out.println("\nMaintenanceClient: *************** Remove a Facility  from MaintenanceRequest and Schedule *************************");
        maintainenceService.RemoveRequest(3); //scheduleid



    }
}
